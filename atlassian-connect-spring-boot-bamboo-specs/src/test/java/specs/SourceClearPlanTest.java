package specs;

import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.junit.Test;

public class SourceClearPlanTest {

    @Test
    public void shouldValidatePlan() {
        Plan plan = new SourceClearPlan().plan();
        EntityPropertiesBuilders.build(plan).validate();
    }
}
