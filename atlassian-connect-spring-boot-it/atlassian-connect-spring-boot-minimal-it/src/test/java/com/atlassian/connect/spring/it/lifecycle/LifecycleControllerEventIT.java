package com.atlassian.connect.spring.it.lifecycle;

import com.atlassian.connect.spring.AddonInstalledEvent;
import com.atlassian.connect.spring.AddonUninstalledEvent;
import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.it.util.AtlassianHostBuilder;
import com.atlassian.connect.spring.it.util.AtlassianHosts;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import com.atlassian.connect.spring.it.util.LifecycleBodyHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TransferQueue;

import static com.atlassian.connect.spring.it.util.AtlassianHosts.CLIENT_KEY;
import static com.atlassian.connect.spring.it.util.AtlassianHosts.SHARED_SECRET;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LifecycleControllerEventIT extends BaseApplicationIT {

    @Before
    public void setUp() {
        addonLifecycleEventCollector.clearEvents();
    }

    @Autowired
    private LifecycleEventCollector addonLifecycleEventCollector;

    @Test
    public void shouldFireEventOnInstall() throws Exception {
        mvc.perform(post("/installed")
                .contentType(MediaType.APPLICATION_JSON)
                .content(LifecycleBodyHelper.createLifecycleJson("installed", SHARED_SECRET)))
                .andExpect(status().isNoContent());

        AtlassianHost installedHost = hostRepository.findById(CLIENT_KEY).get();
        AddonInstalledEvent installedEvent = addonLifecycleEventCollector.takeInstalledEvent();
        assertThat(installedEvent.getHost(), equalTo(installedHost));
    }

    @Test
    public void shouldFireEventOnUninstall() throws Exception {
        AtlassianHost host = new AtlassianHostBuilder().clientKey(CLIENT_KEY).sharedSecret(SHARED_SECRET)
                .baseUrl(AtlassianHosts.BASE_URL).build();
        hostRepository.save(host);
        setJwtAuthenticatedPrincipal(host);
        mvc.perform(post("/uninstalled")
                .contentType(MediaType.APPLICATION_JSON)
                .content(LifecycleBodyHelper.createLifecycleJson("uninstalled", SHARED_SECRET)))
                .andExpect(status().isNoContent());

        AtlassianHost installedHost = hostRepository.findById(CLIENT_KEY).get();
        AddonUninstalledEvent uninstalledEvent = addonLifecycleEventCollector.takeUninstalledEvent();
        assertThat(uninstalledEvent.getHost(), equalTo(installedHost));
    }

    @TestConfiguration
    public static class EventCollectorConfiguration {

        @Bean
        public LifecycleEventCollector lifecycleEventCollector() {
            return new LifecycleEventCollector();
        }
    }

    @TestComponent
    public static class LifecycleEventCollector {

        private static final int POLL_TIMEOUT_SECONDS = 5;

        private TransferQueue<AddonInstalledEvent> installedEvents = new LinkedTransferQueue<>();

        private TransferQueue<AddonUninstalledEvent> uninstalledEvents = new LinkedTransferQueue<>();

        @EventListener
        public void handleAddonInstalledEvent(AddonInstalledEvent installedEvent) throws InterruptedException {
            installedEvents.add(installedEvent);

            // An exception thrown here will not fail the request to the /installed resource
            throw new LifecycleEventException();
        }

        @EventListener
        public void handleAddonUninstalledEvent(AddonUninstalledEvent uninstalledEvent) throws InterruptedException {
            uninstalledEvents.add(uninstalledEvent);

            // An exception thrown here will not fail the request to the /uninstalled resource
            throw new LifecycleEventException();
        }

        public AddonInstalledEvent takeInstalledEvent() throws InterruptedException {
            AddonInstalledEvent event = installedEvents.poll(POLL_TIMEOUT_SECONDS, TimeUnit.SECONDS);
            assert installedEvents.isEmpty();
            return event;
        }

        public AddonUninstalledEvent takeUninstalledEvent() throws InterruptedException {
            AddonUninstalledEvent event = uninstalledEvents.poll(POLL_TIMEOUT_SECONDS, TimeUnit.SECONDS);
            assert uninstalledEvents.isEmpty();
            return event;
        }

        public void clearEvents() {
            installedEvents.clear();
            uninstalledEvents.clear();
        }
    }

    public static class LifecycleEventException extends RuntimeException {}
}
