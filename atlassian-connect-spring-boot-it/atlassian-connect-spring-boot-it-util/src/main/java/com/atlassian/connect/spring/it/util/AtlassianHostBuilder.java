package com.atlassian.connect.spring.it.util;

import com.atlassian.connect.spring.AtlassianHost;

public class AtlassianHostBuilder {

    private String clientKey = AtlassianHosts.CLIENT_KEY;

    private String publicKey = AtlassianHosts.PUBLIC_KEY;

    private String oauthClientId = "some-client-id";

    private String sharedSecret = AtlassianHosts.SHARED_SECRET;

    private String baseUrl = AtlassianHosts.BASE_URL;

    private String productType = AtlassianHosts.PRODUCT_TYPE;

    private String description = "Test host";

    private String serviceEntitlementNumber = null;

    private boolean addonInstalled = true;

    public AtlassianHostBuilder() {}

    public AtlassianHostBuilder clientKey(String clientKey) {
        this.clientKey = clientKey;
        return this;
    }

    public AtlassianHostBuilder publicKey(String publicKey) {
        this.publicKey = publicKey;
        return this;
    }

    public AtlassianHostBuilder oauthClientId(String oauthClientId) {
        this.oauthClientId = oauthClientId;
        return this;
    }

    public AtlassianHostBuilder sharedSecret(String sharedSecret) {
        this.sharedSecret = sharedSecret;
        return this;
    }

    public AtlassianHostBuilder baseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
        return this;
    }

    public AtlassianHostBuilder productType(String productType) {
        this.productType = productType;
        return this;
    }

    public AtlassianHostBuilder description(String description) {
        this.description = description;
        return this;
    }

    public AtlassianHostBuilder serviceEntitlementNumber(String serviceEntitlementNumber) {
        this.serviceEntitlementNumber = serviceEntitlementNumber;
        return this;
    }

    public AtlassianHostBuilder addonInstalled(boolean addonInstalled) {
        this.addonInstalled = addonInstalled;
        return this;
    }

    public AtlassianHost build() {
        AtlassianHost host = new AtlassianHost();
        host.setClientKey(clientKey);
        host.setPublicKey(publicKey);
        host.setOauthClientId(oauthClientId);
        host.setSharedSecret(sharedSecret);
        host.setBaseUrl(baseUrl);
        host.setProductType(productType);
        host.setDescription(description);
        host.setServiceEntitlementNumber(serviceEntitlementNumber);
        host.setAddonInstalled(addonInstalled);
        return host;
    }
}
